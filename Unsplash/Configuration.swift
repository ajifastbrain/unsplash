//
//  Configuration.swift
//  Mandiri
//
//  Created by Aji Prakosa on 31/05/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation

enum SchemeConfiguration: String {
    case Staging        = "Staging"
    case Release        = "Release"
    case ReleaseStaging = "Release-Staging"
}

private let kConfiguration      = "Configuration"
private let kAPIBaseURL         = "APIBaseURL"
private let kAPIGetHoldr        = "APIGetHoldr"
private let kGATrackingID       = "GATrackingID"
private let kAuthorization      = "Authorization"

class Configuration: NSObject {
    
    let variables: NSDictionary
    
    // MARK: Singleton
    class var sharedInstance: Configuration {
        struct Static {
            static let instance = Configuration()
        }
        
        return Static.instance
    }
    
    // MARK: Life Cycle
    override init() {
        let mainBundle = Bundle.main
        let configuration = mainBundle.infoDictionary![kConfiguration] as! String
        
        let path = mainBundle.path(forResource: kConfiguration, ofType: "plist")
        let configurations = NSDictionary(contentsOfFile: path!)
        
        variables = configurations?[configuration] as! NSDictionary
        
        super.init()
    }
    
    // MARK: Class Functions
    class func schemeConfiguration() -> SchemeConfiguration {
        return SchemeConfiguration(rawValue: Bundle.main.infoDictionary![kConfiguration] as! String)!
    }
    
    class func baseURL() -> String {
        let configuration = Configuration.sharedInstance
        
        return configuration.variables[kAPIBaseURL] as! String
    }
    
    class func baseRegistrationURL() -> String {
        let configuration = Configuration.sharedInstance
        
        return configuration.variables[kAPIGetHoldr] as! String
    }
    
    class func GATrackingID() -> String {
        let configuration = Configuration.sharedInstance
        
        return configuration.variables[kGATrackingID] as! String
    }
    
    class func Authorization() -> String {
        let configuration = Configuration.sharedInstance
        
        return configuration.variables[kAuthorization] as! String
    }
}
