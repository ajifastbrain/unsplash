import UIKit
import SideMenu

class MovieListViewController: BaseViewController {
    @IBOutlet weak var mainTable: UITableView!
    
    var presenter:ViewToPresenterProtocol?
    
    var movieList: MovieListModel?
    
    private var numRowx = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Unsplash Images"
        presenter?.startFetchingMovieList()
        self.showLoadingView()
        
        mainTable.register(UINib(nibName: "SearchBarCell", bundle: nil), forCellReuseIdentifier: "SearchBarCell")
        mainTable.register(UINib(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: "MenuCell")
        mainTable.register(UINib(nibName: "DetailViewCell", bundle: nil), forCellReuseIdentifier: "DetailViewCell")
        
        
        mainTable.delegate = self
        mainTable.dataSource = self
        mainTable.separatorStyle = .none
        
    }
 }


extension MovieListViewController: PresenterToViewProtocol{
       
    func showMovieList(movieListArray: MovieListModel?) {
        self.movieList = movieListArray
        self.numRowx = self.movieList?.results?.count ?? 0
        self.mainTable.reloadData()
        
        self.hideLoadingView()
    }
       
    func showError(error: String) {
        self.hideLoadingView()
        self.showAlertWith(title: "FAILED".localized(), message: error, action: {
            _ = self.navigationController?.popViewController(animated: true)
        })
    }
}


extension MovieListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numRows:Int = Int()
        switch section {
        case 0:
            numRows = 1
        case 1:
            numRows = 1
        case 2:
            numRows = numRowx
        default:
            break
        }
        return numRows

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                cell = mainTable.dequeueReusableCell(withIdentifier: "SearchBarCell", for: indexPath as IndexPath) as UITableViewCell
                let cell = cell as! SearchBarCell
                cell.btnMenu.addTarget(self, action: #selector(onOpenMenu(sender:)), for: .touchUpInside)
                cell.selectionStyle = .none
            default:
                break
            }
        case 1:
            switch indexPath.row {
            case 0:
                cell = mainTable.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath as IndexPath) as UITableViewCell
                let cell = cell as! MenuCell
                cell.selectionStyle = .none
            default:
                break
            }
        case 2:
            cell = mainTable.dequeueReusableCell(withIdentifier: "DetailViewCell", for: indexPath as IndexPath) as! DetailViewCell
            let cell = cell as! DetailViewCell
            let titles: String = movieList?.results?[indexPath.row].description ?? ""
            let subTitles: String = movieList?.results?[indexPath.row].alt_description ?? ""
            let URLImgProfiles: String = movieList?.results?[indexPath.row].user?.profile_image?.small ?? ""
            let URLImgMains: String = movieList?.results?[indexPath.row].urls?.regular ?? ""
            
            cell.configureView(title: titles, subTitle: subTitles, URLImgProfile: URLImgProfiles, URLImgMain: URLImgMains)
            cell.selectionStyle = .none
        default:
            break
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var heightForRows: CGFloat = CGFloat()
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                heightForRows = 72
            default:
                break
            }
        case 1:
            switch indexPath.row {
            case 0:
                heightForRows = 317
            default:
                break
            }
        case 2:
            heightForRows = 509
        default:
            break
        }
        
        return heightForRows
    }

    @objc func onOpenMenu(sender: UIButton) {
        let menu = SideMenuNavigationController(rootViewController: SideMenuViewController())
        menu.presentationStyle = .menuDissolveIn
        menu.alwaysAnimate = true
        menu.animationOptions = .curveEaseInOut
        menu.menuWidth = 300
        present(menu, animated: true, completion: nil)
        
    }
    
    
    
    
}
