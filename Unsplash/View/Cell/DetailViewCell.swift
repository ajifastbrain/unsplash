import UIKit
import SDWebImage

class DetailViewCell: UITableViewCell {
    @IBOutlet weak var imageProfile: UIImageView!
    
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var labelSubTitle: UILabel!
    
    @IBOutlet weak var mainImage: UIImageView!
    
    @IBOutlet weak var imageLove: UIImageView!
    
    @IBOutlet weak var btnLove: UIButton!
    
    @IBOutlet weak var btnPlus: UIButton!
    
    @IBOutlet weak var btnArrowDown: UIButton!
    
    @IBOutlet weak var btnDownload: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnLove.layer.borderWidth = 1
        btnLove.layer.borderColor = UIColor.init(hexString: "#dabca3").cgColor
        btnLove.layer.cornerRadius = 8
        
        btnPlus.layer.borderWidth = 1
        btnPlus.layer.borderColor = UIColor.init(hexString: "#dabca3").cgColor
        btnPlus.layer.cornerRadius = 8
        
        btnDownload.layer.borderWidth = 1
        btnDownload.layer.borderColor = UIColor.init(hexString: "#dabca3").cgColor
        btnDownload.layer.cornerRadius = 1
        
        btnArrowDown.addTopBorderWithColor(color: UIColor.init(hexString: "#dabca3"), width: 1)
        btnArrowDown.addRightBorderWithColor(color: UIColor.init(hexString: "#dabca3"), width: 1)
        btnArrowDown.addBottomBorderWithColor(color: UIColor.init(hexString: "#dabca3"), width: 1)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureView(title: String, subTitle: String, URLImgProfile: String, URLImgMain: String) {
        labelTitle.text = title
        labelSubTitle.text = subTitle
        imageProfile.sd_setImage(with: URL(string: URLImgProfile), placeholderImage: nil, options: SDWebImageOptions.retryFailed)
        mainImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        mainImage.sd_setImage(with: URL(string: URLImgMain), placeholderImage: nil, options: SDWebImageOptions.retryFailed)
    }
    
    
}
