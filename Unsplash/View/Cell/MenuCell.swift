import UIKit

class MenuCell: UITableViewCell {
    @IBOutlet weak var mainMenuTable: UICollectionView!
    
    private var cellMenu: [String] = ["Editorial", "Travel", "Sustainability", "Current Events", "COVID-19", "Nature", "Wallpapers", "Textures & Patterns", "People", "Business & Work", "Technology", "Animals", "Interiors", "Architecture", "Food & Drink", "Athletics", "Spirituality", "Health & Wellness", "Film", "Fashion", "Experimental", "Arts & Culture", "History"]
    
    private var selectedOffersIndex: Int = 0
    
    @IBOutlet weak var labelSubTitle: UILabel!
    
    @IBOutlet weak var labelPowered: UILabel!
    
    @IBOutlet weak var labelBottom: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainMenuTable.register(UINib(nibName: "ItemMenuCell", bundle: nil), forCellWithReuseIdentifier: "ItemMenuCell")
        mainMenuTable.delegate = self
        mainMenuTable.dataSource = self
        mainMenuTable.reloadData()
        labelSubTitle.numberOfLines = 0
        labelSubTitle.lineBreakMode = .byWordWrapping
        
        labelPowered.text = labelText.poweredBy.localized()
        labelPowered.textColor = UIColor.init(hexString: "#FFFFFF")
        
        labelBottom.text = labelText.createWeb.localized()
        labelBottom.textColor = UIColor.init(hexString: "#fbfbfb")
        
        labelSubTitle.text = labelText.internetSource.localized() + labelText.usableImages.localized()
        
        let ints = labelText.internetSource.localized()
        let usbi = labelText.usableImages.localized()
        let intsRange = labelSubTitle.text!.range(of: ints)!.nsRange
        let usbiRange = labelSubTitle.text!.range(of: usbi)!.nsRange
        let attributedText = NSMutableAttributedString(string: labelSubTitle.text!)
        attributedText.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 18), range: intsRange)
        attributedText.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 18), range: usbiRange)
        attributedText.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: usbiRange)
        attributedText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.init(hexString: "#FFFFFF"), range: intsRange)
        attributedText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.init(hexString: "#fbfbfb"), range: usbiRange)
        labelSubTitle.attributedText = attributedText
        labelSubTitle.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(usbiClicked(_:))))
        
    }
    
    @objc func usbiClicked(_ tapGesture: UITapGestureRecognizer) {
        let usbiRange = labelSubTitle.text!.range(of: labelText.usableImages.localized())!.nsRange
        if tapGesture.didTapAttributedTextInLabel(label: labelSubTitle, inRange: usbiRange) {
            print("OPEN New VC")
        }
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


extension MenuCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cellMenu.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemMenuCell", for: indexPath as IndexPath) as! ItemMenuCell
        
        cell.labelTitle.text = String(cellMenu[indexPath.row])
        cell.labelTitle.textColor = UIColor.init(hexString: "#000000")
        
        if selectedOffersIndex == indexPath.row {
            cell.viewLink.layer.backgroundColor = UIColor.init(hexString: "#233dff").cgColor
        } else {
            cell.viewLink.layer.backgroundColor = UIColor.init(hexString: "#ffffff").cgColor
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let item = cellMenu[indexPath.row]
        let itemSize = item.size(withAttributes: [
            NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16)
        ])
        return CGSize(width: (itemSize.width * 1.3), height: 61)
    }
    
//   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let widthCell = lengthLabel[indexPath.row]
//      return CGSize(width: widthCell, height: 61)
//    }
//
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath:IndexPath) -> CGFloat {
        return 40
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedOffersIndex = indexPath.row
        mainMenuTable.reloadData()
    }

}
