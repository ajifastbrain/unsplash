import UIKit

class SearchBarCell: UITableViewCell {
    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var btnMenu: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        txtSearch.layer.cornerRadius = 23
        txtSearch.layer.borderWidth = 1
        txtSearch.layer.borderColor = UIColor.init(hexString: "#D0CCCC").cgColor
        txtSearch.setLeftPaddingPoints(50)
        txtSearch.setRightPaddingPoints(16)
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
