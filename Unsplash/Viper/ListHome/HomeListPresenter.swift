//
//  HomeListPresenter.swift
//  Unsplash
//
//  Created by Aji Prakosa on 29/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation
import UIKit

class HomeListPresenter:ViewToPresenterProtocol {
    
    var view: PresenterToViewProtocol?
    
    var interactor: PresenterToInteractorProtocol?
    
    var router: PresenterToRouterProtocol?
    
    func startFetchingMovieList() {
        interactor?.fetchMovieList()
    }
    
    func showMovieListController(navigationController: UINavigationController) {
        router?.pushToMovieScreen(navigationConroller:navigationController)
    }

}

extension HomeListPresenter: InteractorToPresenterProtocol{
    
    func MovieListFetchedSuccess(movieListModelArray: MovieListModel?) {
        view?.showMovieList(movieListArray: movieListModelArray)
    }
    
    func MovieListFetchFailed(error: String) {
        view?.showError(error: error)
    }
    
}
