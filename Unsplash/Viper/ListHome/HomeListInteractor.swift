//
//  HomeListInteractor.swift
//  Unsplash
//
//  Created by Aji Prakosa on 29/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class MovieListInteractor: PresenterToInteractorProtocol{
    var presenter: InteractorToPresenterProtocol?
   
    private var movieListModels: MovieListModel?
    
    func fetchMovieList() {
        API.getMovieList(completion: { (movieList, error) in
            if let movies = movieList {
                self.movieListModels = movies
                self.presenter?.MovieListFetchedSuccess(movieListModelArray: self.movieListModels)
            } else if let error = error {
                self.presenter?.MovieListFetchFailed(error: error)
            }
       })
     }
}
