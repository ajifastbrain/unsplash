//
//  HomeListProtocol.swift
//  Unsplash
//
//  Created by Macintosh on 29/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation
import UIKit

protocol ViewToPresenterProtocol: class{
    
    var view: PresenterToViewProtocol? {get set}
    var interactor: PresenterToInteractorProtocol? {get set}
    var router: PresenterToRouterProtocol? {get set}
    func startFetchingMovieList()
    func showMovieListController(navigationController:UINavigationController)

}

protocol PresenterToViewProtocol: class{
    func showMovieList(movieListArray: MovieListModel?)
    func showError(error: String)
}

protocol PresenterToRouterProtocol: class {
    static func createModule()-> MovieListViewController
    func pushToMovieScreen(navigationConroller:UINavigationController)
}

protocol PresenterToInteractorProtocol: class {
    var presenter:InteractorToPresenterProtocol? {get set}
    func fetchMovieList()
}

protocol InteractorToPresenterProtocol: class {
    func MovieListFetchedSuccess(movieListModelArray: MovieListModel?)
    func MovieListFetchFailed(error: String)
}
