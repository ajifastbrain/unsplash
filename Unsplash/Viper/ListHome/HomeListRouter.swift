//
//  HomeListRouter.swift
//  Unsplash
//
//  Created by Aji Prakosa on 29/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation
import UIKit

class HomeListRouter:PresenterToRouterProtocol{
    
    static func createModule() -> MovieListViewController {
        
        let view = MovieListViewController()
        
        let presenter: ViewToPresenterProtocol & InteractorToPresenterProtocol = HomeListPresenter()
        let interactor: PresenterToInteractorProtocol = MovieListInteractor()
        let router:PresenterToRouterProtocol = HomeListRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
        
    }
    
    
    func pushToMovieScreen(navigationConroller navigationController:UINavigationController) {
        
        let movieModue = MovieListViewController()
        navigationController.pushViewController(movieModue,animated: true)
        
    }
    
}
