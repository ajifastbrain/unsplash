import ObjectMapper

class MovieListModel: Mappable {
    var total: Int?
    var total_pages: Int?
    var results: [result]?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        results <- map["results"]
    }
}


class result: Mappable {
    var id: String?
    var created_at: String?
    var updated_at: String?
    var promoted_at: String?
    var width: Int?
    var height: Int?
    var color: String?
    var description: String?
    var alt_description: String?
    var urls: urls?
    var links: links?
    var likes: Int?
    var liked_by_user: Bool?
    var user: user?
    var tags: [tags]?
    
    required init?(map: Map){
    }

    func mapping(map: Map) {
        id <- map["id"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        promoted_at <- map["promoted_at"]
        width <- map["width"]
        height <- map["height"]
        color <- map["color"]
        description <- map["description"]
        alt_description <- map["alt_description"]
        urls <- map["urls"]
        links <- map["links"]
        likes <- map["likes"]
        liked_by_user <- map["liked_by_user"]
        user <- map["user"]
        tags <- map["tags"]
    }
}

class urls: Mappable {
    var raw: String?
    var full: String?
    var regular: String?
    var small: String?
    var thumb: String?
    
    required init?(map: Map){
    }

    func mapping(map: Map) {
        raw <- map["raw"]
        full <- map["full"]
        regular <- map["regular"]
        small <- map["small"]
        thumb <- map["thumb"]
    }
}

class links: Mappable {
    var selfs: String?
    var html: String?
    var download: String?
    var download_location: String?
    
    required init?(map: Map){
    }

    func mapping(map: Map) {
        selfs <- map["self"]
        html <- map["html"]
        download <- map["download"]
        download_location <- map["download_location"]
    }

}

class user: Mappable {
    var id: String?
    var updated_at: String?
    var username: String?
    var name: String?
    var first_name: String?
    var last_name: String?
    var twitter_username: String?
    var portfolio_url: String?
    var bio: String?
    var location: String?
    var links_user: links_user?
    var profile_image: profile_image?
    var instagram_username: String?
    var total_likes: String?
    var total_photos: String?
    var accepted_tos: Bool?
    
    required init?(map: Map){
    }

    func mapping(map: Map) {
        id <- map["id"]
        updated_at <- map["updated_at"]
        username <- map["username"]
        name <- map["name"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        twitter_username <- map["twitter_username"]
        portfolio_url <- map["portfolio_url"]
        bio <- map["bio"]
        location <- map["location"]
        links_user <- map["links_user"]
        profile_image <- map["profile_image"]
        instagram_username <- map["instagram_username"]
        total_likes <- map["total_likes"]
        total_photos <- map["total_photos"]
        accepted_tos <- map["accepted_tos"]
    }
}

class links_user: Mappable {
    var selfs: String?
    var html: String?
    var photos: String?
    var likes: String?
    var portfolio: String?
    var following: String?
    var followers: String?
    
    required init?(map: Map){
    }

    func mapping(map: Map) {
        selfs <- map["self"]
        html <- map["html"]
        photos <- map["photos"]
        likes <- map["likes"]
        portfolio <- map["portfolio"]
        following <- map["following"]
        followers <- map["followers"]
    }
}


class profile_image: Mappable {
    var small: String?
    var medium: String?
    var large: String?
    
    required init?(map: Map){
    }

    func mapping(map: Map) {
        small <- map["small"]
        medium <- map["medium"]
        large <- map["large"]
    }
}

class tags: Mappable {
    var type: String?
    var title: String?
    var source: source?
    
    required init?(map: Map){
    }

    func mapping(map: Map) {
        type <- map["type"]
        title <- map["title"]
        source <- map["source"]
    }
}

class source: Mappable {
    var ancestry: ancestry?
    var title: String?
    var subtitle: String?
    var description: String?
    var meta_title: String?
    var meta_description: String?
    var cover_photo: cover_photo?
    
    required init?(map: Map){
    }

    func mapping(map: Map) {
        ancestry <- map["ancestry"]
        title <- map["title"]
        subtitle <- map["subtitle"]
        description <- map["description"]
        meta_title <- map["meta_title"]
        meta_description <- map["meta_description"]
        cover_photo <- map["cover_photo"]
    }
}

class ancestry: Mappable {
    var type: type?
    var category: category?
    
    required init?(map: Map){
    }

    func mapping(map: Map) {
        type <- map["type"]
        category <- map["category"]
    }

}

class type: Mappable {
    var slug: String?
    var pretty_slug: String?
    
    required init?(map: Map){
    }

    func mapping(map: Map) {
        slug <- map["slug"]
        pretty_slug <- map["pretty_slug"]
    }
}

class category: Mappable {
    var slug: String?
    var pretty_slug: String?
    
    required init?(map: Map){
    }

    func mapping(map: Map) {
        slug <- map["slug"]
        pretty_slug <- map["pretty_slug"]
    }
}

class cover_photo: Mappable {
    var id: String?
    var created_at: String?
    var updated_at: String?
    var promoted_at: String?
    var width: Int?
    var height: Int?
    var color: String?
    var description: String?
    var alt_description: String?
    var urls: urls_cover?
    var links: links_cover?
    var likes: Int?
    var liked_by_user: Bool?
    var user: user_cover?
    
    required init?(map: Map){
    }

    func mapping(map: Map) {
        id <- map["id"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        promoted_at <- map["promoted_at"]
        width <- map["width"]
        height <- map["height"]
        color <- map["color"]
        description <- map["description"]
        alt_description <- map["alt_description"]
        urls <- map["urls"]
        links <- map["links"]
        likes <- map["likes"]
        liked_by_user <- map["liked_by_user"]
        user <- map["user"]
    }
}

class urls_cover: Mappable {
    var raw: String?
    var full: String?
    var regular: String?
    var small: String?
    var thumb: String?
    
    required init?(map: Map){
    }

    func mapping(map: Map) {
        raw <- map["raw"]
        full <- map["full"]
        regular <- map["regular"]
        small <- map["small"]
        thumb <- map["thumb"]
    }
}

class links_cover: Mappable {
    var selfs: String?
    var html: String?
    var download: String?
    var download_location: String?
    
    required init?(map: Map){
    }

    func mapping(map: Map) {
        selfs <- map["self"]
        html <- map["html"]
        download <- map["download"]
        download_location <- map["download_location"]
    }
}


class user_cover: Mappable {
    var id: String?
    var updated_at: String?
    var username: String?
    var name: String?
    var first_name: String?
    var last_name: String?
    var twitter_username: String?
    var portfolio_url: String?
    var bio: String?
    var location: String?
    var links: links_user_cover?
    var profile_image: profile_image_user_cover?
    var instagram_username: String?
    var total_collections: Int?
    var total_likes: Int?
    var total_photos: Int?
    var accepted_tos: Bool?
    
    required init?(map: Map){
    }

    func mapping(map: Map) {
        id <- map["id"]
        updated_at <- map["updated_at"]
        username <- map["username"]
        name <- map["name"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        twitter_username <- map["twitter_username"]
        portfolio_url <- map["portfolio_url"]
        bio <- map["bio"]
        location <- map["location"]
        links <- map["links"]
        profile_image <- map["profile_image"]
        instagram_username <- map["instagram_username"]
        total_collections <- map["total_collections"]
        total_likes <- map["total_likes"]
        total_photos <- map["total_photos"]
        accepted_tos <- map["total_photos"]
    }

}

class links_user_cover: Mappable {
    var selfs: String?
    var html: String?
    var photos: String?
    var likes: String?
    var portfolio: String?
    var following: String?
    var followers: String?
    
    required init?(map: Map){
    }

    func mapping(map: Map) {
        selfs <- map["self"]
        html <- map["html"]
        photos <- map["photos"]
        likes <- map["likes"]
        portfolio <- map["portfolio"]
        following <- map["following"]
        followers <- map["followers"]
    }
}

class profile_image_user_cover: Mappable {
    var small: String?
    var medium: String?
    var large: String?
    
    required init?(map: Map){
    }

    func mapping(map: Map) {
        small <- map["small"]
        medium <- map["medium"]
        large <- map["large"]
    }
}
