import ObjectMapper

class TokenModel: Mappable {
    var success: Int?
    var expires_at: String?
    var request_token: String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        success <- map["success"]
        expires_at <- map["expires_at"]
        request_token <- map["request_token"]
    }
    
}
