//
//  API.swift
//  Mandiri
//
//  Created by Aji Prakosa on 31/05/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class API: NSObject {
    
    class func resultProcessor(response: (DataResponse<Any>), completion: (_ data: AnyObject?, _ error: String?) -> Void) {
        var apiError: String?
        var body: AnyObject?
        
        switch response.result {
        case .failure(let error):
            Logger.log("Error coy: \(error)")
            apiError = error.localizedDescription
        case .success(let json):
            Logger.log(json)
            if let httpResponse: HTTPURLResponse = response.response, httpResponse.statusCode == 200 || httpResponse.statusCode == 201 || httpResponse.statusCode == 204 {
                body = json as AnyObject?
            } else {
                if let JSON = json as? [ String : AnyObject ] {
                    if let errorDesc = JSON["error_description"] {
                        apiError = errorDesc as? String
                    } else if let errorMessage = JSON["message"] {
                        if errorMessage is [ String : AnyObject ] {
                            if let message = errorMessage["error"] as? String {
                                apiError = message
                            } else if let message = errorMessage["error_message"] as? String {
                                apiError = message
                            }
                        } else {
                            apiError = errorMessage as? String
                        }
                    } else if let error = JSON["error"] as? String {
                        apiError = error
                    } else {
                        apiError = "SOMETHING_WENT_WRONG".localized()
                    }
                } else {
                    apiError = "SOMETHING_WENT_WRONG".localized()
                }
            }
        }
        
        completion(body, apiError)
    }
    
    class func getAuthToken(completion: @escaping (_ token: TokenModel?, _ error: String?) -> Void) {
        let url = Configuration.baseURL() + APIEndPoint.token
        let defaults = UserDefaults.standard
        Logger.log(url)
        
        NetworkManager.request(.get, url, encoding: JSONEncoding.default, completion: { (response) in
            API.resultProcessor(response: response, completion: { (json, error) in
                var token: TokenModel?
                var apiError: String?
                if let json = json {
                    token = Mapper<TokenModel>().map(JSONString: stringify(json: json))
                    defaults.set(token, forKey: defaultsKeys.keyAccessToken)
                } else if let err = error {
                    apiError = err
                }
                
                completion(token, apiError)
            })
        })
    }
    
    class func getMovieList(completion: @escaping (_ movieList: MovieListModel?, _ error: String?) -> Void) {
        let url = Configuration.baseURL() + APIEndPoint.movieList
        Logger.log(url)
        
        NetworkManager.request(.get, url, encoding: JSONEncoding.default, completion: { (response) in
            API.resultProcessor(response: response, completion: { (json, error) in
                var movieList: MovieListModel?
                var apiError: String?
                if let json = json {
                    movieList = Mapper<MovieListModel>().map(JSONString: stringify(json: json))
                } else if let err = error {
                    apiError = err
                }
                completion(movieList, apiError)
            })
        })
    }

    static func stringify(json: Any, prettyPrinted: Bool = false) -> String {
        var options: JSONSerialization.WritingOptions = []
        if prettyPrinted {
          options = JSONSerialization.WritingOptions.prettyPrinted
        }

        do {
          let data = try JSONSerialization.data(withJSONObject: json, options: options)
          if let string = String(data: data, encoding: String.Encoding.utf8) {
            return string
          }
        } catch {
          print(error)
        }

        return ""
    }

}

