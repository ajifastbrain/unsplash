//
//  Constant.swift
//  Mandiri
//
//  Created by Macintosh on 31/05/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation

struct labelText {
    static let internetSource = "The internets source of "
    static let usableImages = "freely-usable images."
    static let poweredBy = "Powered by creators everywhere."
    static let createWeb = "All you need to create a website."
    
}

struct MandiriNotification {
    static let tokenExpired     = "NOTIFICATION_TOKEN_EXPIRED"
    static let pinChanged       = "NOTIFICATION_PIN_CHANGED"
}

struct APIKey {
    static let key = "Client-ID d8a272c480b258b875d82f4062d6c52e4ae7f4b4656add778d71e9b638b2f8be"
}

struct APIEndPoint {
    static let token         = "/authentication/token/new?api_key="
    static let movieList     = "/search/photos?page=1&amp;query=office"
    
}
