//
//  NetworkManager.swift
//  Mandiri
//
//  Created by Aji Prakosa on 30/05/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit
import Alamofire

class NetworkManager: NSObject {
    
    class func request(_ method: HTTPMethod, _ URLString: URLConvertible, parameters: [String: AnyObject]? = nil, encoding: ParameterEncoding = URLEncoding.default, useBasicToken: Bool? = false, logoutIf401: Bool = true, completion: @escaping (DataResponse<Any>) -> Void) {
        
        let manager = Alamofire.SessionManager.default
        var headers = [String : String]()
        if useBasicToken! {
            headers["Authorization"] = "\(Configuration.Authorization())"
        } else {
            headers["Authorization"] = (Session.sharedInstance.token != nil)
                ? "\(Session.sharedInstance.token!.success) \(Session.sharedInstance.token!.request_token)"
                : "\(Configuration.Authorization())"
        }
        //Logger.log(headers)
        
        let req = manager.request(URLString, method: method, parameters: parameters, encoding: encoding, headers: headers)
        req.responseString { (response) in
            if let response = response.response {
                Logger.log(response)
                if response.statusCode == 401 && logoutIf401
                {
                    NotificationCenter.default.post(name: NSNotification.Name(MandiriNotification.tokenExpired), object: nil)
                    
                    // USER SIGN OUT TODO
                    print("User Sign Out TODO")
                    return
                    
                } else {
                    req.responseJSON { (response) -> Void in
                        completion(response)
                    }
                }
            } else {
                req.responseJSON { (response) -> Void in
                    completion(response)
                }
            }
        }
    }
}
