//
//  Session.swift
//  Mandiri
//
//  Created by Aji Prakosa on 31/05/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation
import Alamofire

class Session: NSObject {
    static var sharedInstance: Session {
        struct Static {
            static let instance = Session()
        }
        
        return Static.instance
    }
    var token: TokenModel?
}
