//
//  BaseViewController.swift
//  Mandiri
//
//  Created by Macintosh on 31/05/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    let defaults = UserDefaults.standard

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    func showLoadingView() {
        if let window = UIApplication.shared.keyWindow {
            LoadingOverlay.shared.showOverlay(inView: window)
        } else {
            LoadingOverlay.shared.showOverlay(inView: self.navigationController!.view)
        }
    }
    
    func hideLoadingView() {
        LoadingOverlay.shared.hideOverlayView()
    }

    public func showAlertWith(title: String, message: String, action: (() -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "OK".localized().uppercased(), style: UIAlertAction.Style.cancel, handler: { (alertAction) in
            if let action = action {
                action()
            }
        })
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }

}
