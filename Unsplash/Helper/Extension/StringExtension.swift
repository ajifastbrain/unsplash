import Foundation
import Localize_Swift

extension String {
    
    func localized() ->String {
        let flags = Localize.currentLanguage()
        let path = Bundle.main.path(forResource: flags, ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
    
}
